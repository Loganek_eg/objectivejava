package Zadanie6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 6:
    Napisz klasę QuadraticEquation (obliczanie równania kwadratowego)
    o trzech polach typu double a,b,c odpowiednim konstruktorze i metodach:
        - public double calculateDelta(),
        - public double calculateX1(),
        - public double calculateX2().

         */
        Scanner sc = new Scanner(System.in);
        QuadraticEquation quadraticEquation = new QuadraticEquation();
        String inputLine;
        do {
            System.out.println("Podaj liczbę:a,b,c");
            inputLine = sc.nextLine();
            String[] wartosci = inputLine.split(" ");


            double firstNumber = Double.parseDouble(wartosci[0]);
            double secondNumber = Double.parseDouble(wartosci[1]);
            double thirdNumber = Double.parseDouble(wartosci[2]);

            quadraticEquation.setA(firstNumber);
            quadraticEquation.setB(secondNumber);
            quadraticEquation.setC(thirdNumber);
            if ((quadraticEquation.calculateDelta()) > 0) {
                System.out.println("x1= " + quadraticEquation.calculatex1());
                System.out.println("x2= " + quadraticEquation.calculatex2());
                System.out.println("Delta= " + quadraticEquation.calculateDelta());
            } else if ((quadraticEquation.calculateDelta()) == 0) {
                System.out.println("x1= " + quadraticEquation.calculatex1());
                System.out.println("Delta= " + quadraticEquation.calculateDelta());
            } else if ((quadraticEquation.calculateDelta()) < 0) {
                System.out.println("Nie ma rozwiązań");
            }

        } while (!inputLine.equals("quit"));
    }
}
