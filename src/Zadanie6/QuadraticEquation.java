package Zadanie6;

public class QuadraticEquation {
    /*
Zadanie 6:
Napisz klasę QuadraticEquation (obliczanie równania kwadratowego)
o trzech polach typu double a,b,c odpowiednim konstruktorze i metodach:
- public double calculateDelta(),
- public double calculateX1(),
- public double calculateX2().

 */
    // Delta=b^{2}-4ac
    //ax^{2}+bx+c=0
    //double pierwiastek = Math.sqrt(jakasWartosc);
    private double a;
    private double b;
    private double c;

    public double calculateDelta() {
        double delta = (Math.pow(b, 2)) + (-4 * (a * c));
        return delta;
    }

    public double calculatex1() {
        double x1 = (-b - Math.sqrt(calculateDelta())) / (a * 2);
        return x1;
    }

    public double calculatex2() {
        double x2 = (-b + (Math.sqrt(calculateDelta()))) / (a * 2);
        return x2;
    }


    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }
}
