package Zadanie2;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 2:
    Utwórz klasę TeddyBear, który będzie miał pole prywatne String name;
    Dodaj konstruktor, który przyjmie za parametr imię.
    Co to znaczy że pole jest prywatne?
    Dodaj metodę, która wyświetli imię misia;
         */

        TeddyBear teddyBear1 = new TeddyBear("Hugo");
        System.out.println(teddyBear1.getName());
    }
}
