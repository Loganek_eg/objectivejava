package Zadanie34;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /*
        Zadanie 34:
    Stwórz program a w nim enum'a Plec, a w nim dwie wartości: "Kobieta", "Mezczyzna"
    Stwórz program który skanuje z wejścia linie, a następnie sprawdza czy wprowadzona linia zawiera wartości enuma:
            (posługując się parsowaniem enuma rzucającego IllegalArgumentException)
            podpowiedz: String linia = scanner.nextLine();
                        Plec wpisana = Plec.valueOf(linia.trim());
                        System.out.println("Wpisano:" + wpisana);
         */

        Scanner sc = new Scanner(System.in);
        String inputLine="";
        do {
            inputLine = sc.nextLine();
            try {
                Plec wpisana = Plec.valueOf(inputLine.trim().toLowerCase());
                System.out.println("Wpisano:" + wpisana);
            } catch (IllegalArgumentException iae) {
                System.out.println("Wpisana zła wartość" + iae);
            }
        }while(!inputLine.equals("quit"));
    }
}
