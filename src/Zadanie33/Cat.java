package Zadanie33;

public class Cat implements Animal {

    protected String dzwiek;

    public Cat(String dzwiek) {
        this.dzwiek = dzwiek;
    }

    @Override
    public String makeNoise() {
      return dzwiek;


    }

    @Override
    public String toString() {
        return "Cat{" +
                "dzwiek='" + dzwiek + '\'' +
                '}';
    }
}
