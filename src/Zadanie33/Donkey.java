package Zadanie33;

public class Donkey implements Animal {
    protected String dzwiek;
    @Override
    public String makeNoise() {
        return dzwiek;
    }

    public Donkey(String dzwiek) {
        this.dzwiek = dzwiek;
    }

    @Override
    public String toString() {
        return "Donkey{" +
                "dzwiek='" + dzwiek + '\'' +
                '}';
    }
}
