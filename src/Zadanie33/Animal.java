package Zadanie33;

public interface Animal {
    /*
    Zadanie 33:
    Utwórz interfejs Animal, który będzie zawierał metodę makeNoise();\
    Utwórz klasę Dog i Cat, które będą implementowały ten interfejs za pomocą implements
     */

    public String makeNoise();

}
