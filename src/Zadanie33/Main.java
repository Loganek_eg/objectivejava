package Zadanie33;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        Dog dog1 = new Dog("Hał Hał");
        Donkey donkey1 = new Donkey("Ioooo Ioooo");
        Cat cat1 = new Cat("Miauuu");
        List<Animal> listaZwierzat = new ArrayList<>();
        listaZwierzat.add(dog1);
        listaZwierzat.add(donkey1);
        listaZwierzat.add(cat1);
        for (Animal animal : listaZwierzat) {
            System.out.println(animal.makeNoise());

        }


    }
}
