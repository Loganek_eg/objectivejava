package Zadanie33;

public class Dog implements Animal {

    protected String dzwiek;
    public Dog(String dzwiek) {
        this.dzwiek = dzwiek;
    }
    @Override
    public String makeNoise() {
        return dzwiek;

    }

    @Override
    public String toString() {
        return "Dog{" +
                "dzwiek='" + dzwiek + '\'' +
                '}';
    }
}
