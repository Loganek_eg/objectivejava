package Zadanie1;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 1:
    Dodaj klasę Osoba o polach imie i wiek.
    Utwórz 3 osoby o różnych imionach i wieku.
    Wypisz ich dane w postaci:
    “Jestem <imie>, mam <wiek> lat.”

        a) stwórz do tego metodę printYourNameAndSurname
        b) wypisz wartości używając metody toString
        c) wypisz dane używając metod getterów i setterów

         */
        Person person1 = new Person("Tomasz", 26);
        Person person2 = new Person("Kasia", 45);
        Person person3 = new Person("Basia", 21);
        System.out.println("Jestem " + person1.giveMeYourName() + ", mam " + person1.giveMeYourAge() + " lat.");
        System.out.println("Jestem " + person2.giveMeYourName() + ", mam " + person2.giveMeYourAge() + " lat.");
        System.out.println("Jestem " + person3.giveMeYourName() + ", mam " + person3.giveMeYourAge() + " lat.");


    }
}
