package Zadanie4Przykład;

public class Kalkulator {

    public int addTwoNumbers(int numberOne, int numberTwo) {
        int result = numberOne + numberTwo;

        return result;
    }

    public int substractTwoNumbers(int numberOne, int numberTwo) {
        int result = numberOne - numberTwo;

        return result;
    }

    public int multiplyTwoNumbers(int numberOne, int numberTwo) {
        int result = numberOne * numberTwo;

        return result;
    }

    public int divideTwoNumbers(int numberOne, int numberTwo) {
        if (numberTwo != 0) {
            int result = numberOne / numberTwo;

            return result;
        } else {
            System.out.println("You cannot divide by 0");
            return 0;
        }
    }
}

