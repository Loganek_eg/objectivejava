package Zadanie25;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 25:
        a. Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
        b. Wypisz liczbę elementów na ekran (metoda size())
        c. Wypisz wszystkie zbioru elementy na ekran (forEach)
                d. Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)

        e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru. (boolean containDuplicates(String text))
        f. Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
                {(1,2), (2,1), (1,1), (1,2)}.
        g. Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?

        h*. Napisz metodę, która przyjmuje dowolną liczbę liczb typu double (varargs) i tworzy zbiór (HashSet), składający się z tych elementów, lub wyrzuca IllegalArgumentException, jeżeli elementy nie są unikalne
        public static Set<Double> newSet(double numbers…)
        - spróbuj przerobić metodę na wersję generyczną
        public static <T> Set<T> newSet(T... data)
        */


        Integer[] tablica = {10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};

        System.out.println("Tablica :" + tablica);
        List<Integer> lista = Arrays.asList(tablica);
        Set<Integer> set = new HashSet<>();
        set.addAll(lista);
        System.out.println("Zbiór" + set);
        // c. Wypisz wszystkie zbioru elementy na ekran (forEach)
        System.out.print("Zawartość zbioru: ");
        for (Integer value : set) {

            System.out.print(value + ", ");
        }

        //d. Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
        set.remove(10);
        set.remove(12);
        System.out.println();
        System.out.println("Po usunieciu 10 i 12" + set);


//        e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru.\
// (boolean containDuplicates(String text))
        //    f. Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
        //          {(1,2), (2,1), (1,1), (1,2)}.
        //  g. Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?


        //Wypisz liczbę elementów na ekran (metoda size())


    }

    public static Integer size() {
        int licznik = 0;
        for (int i = 0; i < licznik; licznik++) {

        }
        return licznik;
    }
}
