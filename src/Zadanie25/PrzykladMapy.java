package Podstawy.Zadanie25;



import java.util.HashMap;
import java.util.Map;

class PrzykladMapa {
    public static void main(String[] args) {
//        HashMap<String, Integer> map = new HashMap<>();
        Map<String, Integer> map = new HashMap<>();

        map.put("klucz", 2000);
        map.put("klucz1", 40900);
        map.put("klucz2", 40800);
        map.put("klucz3", 40700);
        map.put("klucz4", 46000);
        map.put("klucz5", 60000);

        // iterowanie kluczy
        for(String key: map.keySet()){
            System.out.print (key);
            // wyjecie z mapy elementu pod kluczem key
            System.out.println(" Wartosc: " + map.get(key));

        }

        System.out.println( " >>>>> ");
        // iterowanie wartości
        for(Integer value : map.values()){
            System.out.println(" Wartość " + value);
        }

        // przejście po parach klucz wartość

        System.out.println(">>>>>>>");
        for(Map.Entry<String, Integer> entry: map.entrySet()){
            String klucz = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("Klucz: " + entry.getKey() + " wartosc: " + entry.getValue());
        }

/*
Map<String, Person> family = new HashMap<>();
family.put("mama", new Mother("Janina", "Nazwisko", 200));
family.put("dziecko1", new Child("Juzio", "Nazwisko"));
family.put("dziecko2", new Child("Błond", "Nazwisko"));
family.put("dziecko3", new Child("Michau", "Nazwisko"));

for(Map.Entry<String, Person> entry: family.entrySet()){
String klucz = entry.getKey();
Person value = entry.getValue();
System.out.println("Klucz: " + klucz + " wartosc: " + value);
if( value instanceof Child){
Child dzieckoInstance = (Child) value;
dzieckoInstance.playWithStick();
}
}
*/
    }
}