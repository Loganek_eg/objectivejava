import java.awt.*;
import java.util.*;
import java.util.List;

public class PrzykladZbiory {
    public static void main(String[] args) {
//        ArrayList<Integer> list = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(3);
        list.add(3);
        list.add(4);
        list.add(null);
        list.add(4);
        list.add(null);
        list.add(100);

        for (Integer i : list) {
            System.out.println(i);
        }
        System.out.println();

//        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        Set<Integer> set = new LinkedHashSet<>();
        set.addAll(list);
//        set.add(1);
//        set.add(4);
//        set.add(2);
//        set.add(3);
//        set.add(4);

        for (Integer i : set) {
            System.out.println(i);
        }

        List<Rectangle> listOfRectangle = new LinkedList<>();

        Set<Rectangle> rectangles = new LinkedHashSet<>();
        Rectangle prostokat = new Rectangle();
        rectangles.add(new Rectangle());
        rectangles.add(new Rectangle());
//        System.out.println(prostokat);
        rectangles.add(prostokat);



        for (Rectangle i : rectangles) {
            System.out.println(i);
        }


        listOfRectangle.add(prostokat);
        listOfRectangle.add(prostokat);

//        prostokat.setA(20);

        listOfRectangle.add(prostokat);

        System.out.println(">>>>>>>");
        for (Rectangle i : listOfRectangle) {
            System.out.println(i);
        }

    }
}