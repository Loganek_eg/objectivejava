package Zadanie8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double amount;


        BankAccount bankAccount = new BankAccount();
        String inputLine = "";


        System.out.println("Podaj stan konta:");
        try {
            amount = Double.parseDouble(sc.nextLine());
            bankAccount.setAmount(amount);
        }
        catch (NumberFormatException nfe) {
            System.out.println("Zły format");
        }

        do {
            System.out.println("Co chciałbyś zrobić, dodac (add)/(sub)(status)");
            try {
                inputLine = sc.nextLine();
                if (inputLine.equals("status")) {
                    System.out.println(bankAccount.printBankAccountStatus());
                    continue;
                }
                String[] words = inputLine.split(" ", 2);
                String command = words[0];
                String number = words[1];
                double first = Double.parseDouble(number);
                if (command.equals("add")) {
                    bankAccount.addMoney(first);
                } else if (command.equals("sub")) {
                    bankAccount.subtractMoney(first);
                } else {
                    System.out.println("Wrong comand");
                }
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("Brak liczby");

            }catch (NumberFormatException nfe){
                System.out.println("Zła liczba");
            }
        } while (!inputLine.equals("quit"));

    }
}
