package Zadanie8;

public class BankAccount {

    /*
        Zadanie 8:
            Stwórz program a w nim klasę "BankAccount".
            konto bankowe powinno mieć metodę dodawania pieniędzy do konta, oraz odejmowania :
            (metoda addMoney która w parametrze przyjmuje ilosc pieniedzy do dodania oraz subtractMoney
            która jako parametrz przyjmuje ilosc pieniedzy do odjecia).
            Dodaj do klasy metodę "printBankAccountStatus" która powinna wypisywać stan konta.
     */
    private double money;
    public void setAmount(double money){
        this.money = money;
    }

    public String printBankAccountStatus() {
        return "BankAccount{" +
                "money=" + money +
                '}';
    }

    public double addMoney(double quote) {
        return money += quote;

    }

    public double subtractMoney(double quote) {
        return money -= quote;
    }
}
