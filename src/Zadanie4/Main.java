package Zadanie4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /*Zadanie 4:
    Napisz program w którym jest jedna klasa 'Calculator'.
    Klasa reprezentuje model kalkulatora. Klasa powinna posiadać metodę:
        -   addTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dodawania
        -   substractTwoNumbers - która przyjmuje dwa parametry i zwraca wynik odejmowania
        -   multiplyTwoNumbers - która przyjmuje dwa parametry i zwraca wynik mnożenia
        -   divideTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dzielenia

    Wszystkie metody zwracają wartości. Stwórz maina, a w nim jedną instancję klasy Calculator,
     a następnie przetestuj działanie wszystkich metod.
*/
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj liczby: na ktorych wykonam działnie w kolejnosci: Dodawanie, odejmowanie, dzielenie, mnozenie");
        Calculator calc = new Calculator(sc.nextInt(), sc.nextInt());
        System.out.println("Wynik dodawania: " + calc.addTwoNumbers());
        calc = new Calculator(sc.nextInt(), sc.nextInt());
        System.out.println("Wynik odejmowania: " + calc.substractTwoNumbers());
        calc = new Calculator(sc.nextInt(), sc.nextInt());
        System.out.println("Wynik dzielenia: " + calc.divideTwoNumbers());
        calc = new Calculator(sc.nextInt(), sc.nextInt());
        System.out.println("Wynik mnożenia: " + calc.multiplyTwoNumbers());
    }
}
