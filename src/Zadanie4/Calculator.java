package Zadanie4;

import java.util.Scanner;

public class Calculator {
    /*        /*Zadanie 4:
    Napisz program w którym jest jedna klasa 'Calculator'.
    Klasa reprezentuje model kalkulatora. Klasa powinna posiadać metodę:
        -   addTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dodawania
        -   substractTwoNumbers - która przyjmuje dwa parametry i zwraca wynik odejmowania
        -   multiplyTwoNumbers - która przyjmuje dwa parametry i zwraca wynik mnożenia
        -   divideTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dzielenia

    Wszystkie metody zwracają wartości. Stwórz maina, a w nim jedną instancję klasy Calculator,
     a następnie przetestuj działanie wszystkich metod.
*/


    private int num1;
    private int num2;

    public Calculator(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int addTwoNumbers() {
        return num1 + num2;
    }

    public int substractTwoNumbers() {
        if (num2 != 0) {
            return num1 - num2;
        } else {
            System.out.println(num2 + "jest zerem");
            return 0;
        }
    }

    public int multiplyTwoNumbers() {
        return num1 * num2;
    }

    public int divideTwoNumbers() {

        return num1 / num2;
    }

}
