package Zadanie18;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {


            /*
        Zadanie 18 (LISTY - omówimy 06.10.2017) (na przykładzie ArrayList):
        Napisz metody (użyj pętli for i forEach):
            - Liczącą sumę liczb na liście
            - Liczącą iloczyn liczb na liście
            - Liczącą średnią liczb na liście
* Napisz metodę liczącą:
            - Negujący wszystkie wartości logiczne na liście
            - Koniunkcję wartości logicznych na liście
            - Alternatywę wartości logicznych na liście
        Napisz klasę User o polach String name, String password. Utwórz listę typu User, dodaj do niej kilka obiektów, a następnie wypisz.
*/

//        ArrayList<Integer> lista = new ArrayList<>();
//
//        lista.add(10);
//        lista.add(2);
//        lista.add(7);
//        System.out.println("Suma" + giveAmount(lista));
//        System.out.println("Srednia:" + giveAvarage(lista));
//        System.out.println("Iloczyn:" + giveProduct(lista));

        ArrayList<User> usersList = new ArrayList<>(10);

        usersList.add(new User("Paweł", "HasłoPawła"));
        usersList.add(new User("Andrzej", "HasłoAndrzeja"));
        usersList.add(new User("Tomek", "HasłoTomka"));
        System.out.println(usersList);


        int[] tablica = new int[10];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = (int) (Math.random() * 10);
            System.out.print(tablica[i] + ",");
        }
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            lista.add((int) (Math.random() * 10));
        }
        System.out.println("\n" + lista + ",");
        System.out.println("Takich samych liczb:" + giveCompare(tablica, lista));

        ArrayList <Integer> lista2 = new ArrayList<>();
        for (Integer value: lista) {
            lista2.add(value);
        }
      //przypisz liste do tablicy

        int rozmiar = lista2.size();

    }




    private static Integer giveCompare(int[] tablica, ArrayList<Integer> lista) {
        int value = 0;

        for (int i = 0; i < 10; i++) {
            if (tablica[i] == lista.get(i)) {
                value++;
            }
        }
        return value;

    }

    private static Integer giveAmount(ArrayList<Integer> lista) {
        Integer suma = 0;
        for (Integer value : lista) {
            suma += value;
        }
        return suma;
    }

    private static Integer giveAvarage(ArrayList<Integer> lista) {
        Integer avarage = 0;
        for (Integer value : lista) {
            avarage = giveAmount(lista) / (lista.size());
        }
        return avarage;
    }

    private static Integer giveProduct(ArrayList<Integer> lista) {
        Integer product = 1;
        for (Integer value : lista) {
            product *= value;
        }
        return product;
    }
}


