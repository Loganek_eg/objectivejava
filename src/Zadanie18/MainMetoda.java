package Zadanie18;

import java.util.ArrayList;

public class MainMetoda {
    public static void main(String[] args) {
        ArrayList<Integer> lList1 = new ArrayList<>();
        lList1.add(1);
        lList1.add(2);
        lList1.add(3);
        lList1.add(4);

        checkParameters(1, 2, false);
        checkParameters(5, 11, true);
        checkParameters(-2, 2323, true);

        calculateTheSumofTheList(lList1);

        ArrayList<Integer> lList2 = new ArrayList<>();
        lList2.add(24);
        lList2.add(255);
        lList2.add(566);
        lList2.add(1121);

        calculateTheSumofTheList(lList2);
    }

    public static void calculateTheSumofTheList(ArrayList<Integer> pList) {
        int lSum = 0;
        for (int i = 0; i < pList.size(); i++) {
            lSum += pList.get(i);
        }
        System.out.println("Suma liczb na liscie: " + lSum);
    }

    public static void checkParameters(int param1, int param2, boolean param3) {
        System.out.println("P1: " + param1 + " p2: " + param2 + " p3: " + param3);
    }
}