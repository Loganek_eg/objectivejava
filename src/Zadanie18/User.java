package Zadanie18;

public class User {

/*
        Napisz klasę User o polach String name, String password.
        Utwórz listę typu User, dodaj do niej kilka obiektów, a następnie wypisz.
 */
private String name;
private String password;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
