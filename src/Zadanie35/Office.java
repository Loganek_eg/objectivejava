package Zadanie35;

import java.util.ArrayList;
import java.util.List;

public class Office {
    /*
    Stwórz klasę Office która reprezentuje pojedyncze biuro, w którym będą obsługiwani klienci.
        - w klasie office dodaj metodę handle która obsługuje klienta. Parametrem tej metody będzie klient.
        - w metodzie ma się pojawiać komunikat w formacie: "Klient pesel: {pesel} załatwia sprawę {typ_sprawy}"
     */

    public static void handle(Klient klient) {
        System.out.println("Klient o peselu: " + klient.getPesel() + " załatwia sprawę " + klient.getTypSprawy());
    }

    List<Klient> listaKlientow = new ArrayList<>();

    protected void addKlient(Sprawa typsprawy, int pesel) {
        listaKlientow.add(new Klient(typsprawy, pesel));
    }

}
