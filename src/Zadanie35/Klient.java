package Zadanie35;

public class Klient {

    /*    2. Stwórzmy klasę Client. Każdy klient posiada typ sprawy który musi podać oraz swój pesel, dlatego:
        - stwórz konstruktor w klasie klient, która jako parametr przyjmuje typ sprawy oraz pesel i ustawia wartości tych pól w klasie.
        - stwórz gettery oraz settery tych pól.
        */

    private Sprawa typSprawy;
    private int pesel;

    public Klient() {
    }

    public Klient(Sprawa typSprawy, int pesel) {
        this.typSprawy = typSprawy;
        this.pesel = pesel;
    }

    public void typCzynnosci(Sprawa typSprawy, int pesel) {

    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    public Sprawa getTypSprawy() {
        return typSprawy;
    }

    public void setTypSprawy(Sprawa typSprawy) {
        this.typSprawy = typSprawy;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "typSprawy=" + typSprawy +
                ", pesel=" + pesel +
                '}';
    }
}
