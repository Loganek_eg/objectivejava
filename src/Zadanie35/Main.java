package Zadanie35;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
    5. Stwórz maina. W mainie stwórz department, dodaj do niego dwa biura. Zweryfikuj działanie metod dodania i pobrania biura.
     Następnie stwórz obsługiwanie tej funkcjonalności z linii poleceń/konsoli. W konsoli powinniśmy mieć możliwość wpisania
     "dodaj_biuro" które spowoduje dodanie nowego biura.
    -- Jeśli użyto mapy, to zaleca się dopisanie nazwy po "dodaj_biuro" która będzie kluczem, po którym będziemy odnajdować
     biura. Zamysł jest taki, aby - jeśli używamy mapy, to każde biuro ma swoją nazwę i jest po tej nazwie dodawane,
     a następnie wyszukiwane.
    6. Dodaj w mainie czytanie ze scannera nowego klienta:
    obsluga {PESEL} {TYP_SPRAWY} {ID_POKOJU}
    która tworzy nowego klienta, a następnie wykorzystuje klasę department aby pobrać pokój identyfikowany przez
    ID_POKOJU i obsługuje go metodą handle.
  REGISTER, UNREGISTER,CHECK_STATUS
         */
        Scanner sc = new Scanner(System.in);
        String inputLine = "";
        Departament departament = new Departament();
        Klient klient = new Klient();

        do {
            System.out.println("Witaj, podaj komende dodania biura addoffice");
            inputLine = sc.nextLine();
            if (inputLine.equals("addoffice")) {
                departament.addOffice(new Office());
            } else if (inputLine.equals("getoffice")) {
                System.out.println(departament.getOffice(0));
            } else {
                System.out.println("Zła komenda");
                continue;
            }
            System.out.println("Wpisz nowego klienta {Typ sprawy, pesel");
            String[] wprowadzoneWartosci = sc.nextLine().split(" ");
            Sprawa typsprawy = Sprawa.valueOf(wprowadzoneWartosci[0].toUpperCase());
            int pesel = Integer.parseInt(wprowadzoneWartosci[1]);
            Klient klient1 = new Klient(typsprawy, pesel);
            Office.handle(klient1);

        } while (!inputLine.equals("quit"));
    }
}
