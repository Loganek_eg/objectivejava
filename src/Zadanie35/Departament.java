package Zadanie35;

import java.util.ArrayList;
import java.util.List;

public class Departament {
    /*
     4. Stwórz klasę Department która posiada kolekcję (możesz użyć listy lub mapy) biur.
        - dodaj metodę addOffice która jako parametr przyjmuje biuro.
        - dodaj metodę getOffice która służy do pobierania biura. Jeśli użyłeś/łaś listy
        to parametrem tej metody powinien być indeks, natomiast jeśli użyto mapy, to parametrem powinien być typ klucza tej mapy.
     */
    private String biuro;
    private List<Office> listabiur = new ArrayList<>();

    public void addOffice(Office biuro) {
        listabiur.add(biuro);
    }

    public Office getOffice(int i) {
        return listabiur.get(i);
    }
}
