package Zadanie13;

public class Student {

/*
Zadanie 13:
    Stwórz klasę Student która posiada:
        - pole numer indeksu
        - pole imie
        - pole nazwisko
        - listę ocen
        **(- tablicę ocen (stwórz tablicę maksymalnie 100 ocen) - zadanie dodatkowe z tablicą ocen)
        - metody getterów i setterów
        - metodę do obliczania średniej
        - metodę która zwraca true jeśli żadna z ocen w liście/tablicy ocen nie jest 1 ani 2,
            oraz false w przeciwnym razie.

 */

    protected int nrIndeksu;
    protected String imie;
    protected String nazwisko;
    protected int[] listaOcen = new int[10];
    protected int sumaOcen = 0;
    protected boolean zdales = true;

    public double getMean() {
        double srednia = 0;
        double suma = 0;
        listaOcen[0] = 4;
        listaOcen[1] = 3;
        listaOcen[2] = 3;
        listaOcen[3] = 4;
        listaOcen[4] = 5;
        listaOcen[5] = 5;
        listaOcen[6] = 6;
        listaOcen[7] = 3;
        listaOcen[8] = 4;
        listaOcen[9] = 5;

        for (int i = 0; i < listaOcen.length; i++) {
            suma += listaOcen[i];
        }
        return srednia = (suma / listaOcen.length);
    }


    //        - metodę która zwraca true jeśli żadna z ocen w liście/tablicy ocen nie jest 1 ani 2,
    //   oraz false w przeciwnym razie.
    public boolean getTrue() {
        for (int i = 0; i < listaOcen.length; i++) {
            if (listaOcen[i] <= 2 ) {
                zdales = false;
            }
        }return zdales;

    }


    public int getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int[] getListaOcen() {
        return listaOcen;
    }

    public void setListaOcen(int[] listaOcen) {
        this.listaOcen = listaOcen;
    }
}
