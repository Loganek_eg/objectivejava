package Zadanie22_old;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        do {
        Player player1 = new Player("Tomek");
        Player player2 = new Player("Andrzej");
        Deck deck = new Deck();
        Random random = new Random();
        int licznik = 0;
        int licznikWojen = 0;
        //losuj karty

            losujKarty(player1, player2, deck, random);


            while (player1.kartyplayer.size() > 0 && player2.kartyplayer.size() > 0) {
                Stol stol = new Stol();

                stol.kupkaPlayer1.add(player1.kartyplayer.get(0));
                stol.kupkaPlayer2.add(player2.kartyplayer.get(0));
                int wartosckartaplayer1 = stol.kupkaPlayer1.get(0).getWartosc();
                int wartosckartaplayer2 = stol.kupkaPlayer2.get(0).getWartosc();
                Karta kartaplayer1 = stol.kupkaPlayer1.get(0);
                Karta kartaplayer2 = stol.kupkaPlayer2.get(0);
                player1.kartyplayer.remove(0);
                player2.kartyplayer.remove(0);


                if (wartosckartaplayer1 != wartosckartaplayer2) {
                    if (wartosckartaplayer1 > wartosckartaplayer2) {
                        player1.kartyplayer.add(stol.kupkaPlayer2.get(0));
                        player1.kartyplayer.add(stol.kupkaPlayer1.get(0));
                    } else {
                        player2.kartyplayer.add(stol.kupkaPlayer1.get(0));
                        player2.kartyplayer.add(stol.kupkaPlayer2.get(0));
                    }
                    licznik++;

                } else {
                    try {
                        while (wartosckartaplayer1 == wartosckartaplayer2) {
                            stol.kupkaPlayer1.add(0, player1.kartyplayer.get(0));
                            stol.kupkaPlayer1.add(0, player1.kartyplayer.get(1));
                            player1.kartyplayer.remove(0);
                            player1.kartyplayer.remove(0);
                            stol.kupkaPlayer2.add(0, player2.kartyplayer.get(0));
                            stol.kupkaPlayer2.add(0, player2.kartyplayer.get(1));
                            player2.kartyplayer.remove(0);
                            player2.kartyplayer.remove(0);
                            Thread.sleep(50);

                            wartosckartaplayer2 = stol.kupkaPlayer2.get(0).getWartosc();
                            wartosckartaplayer1 = stol.kupkaPlayer1.get(0).getWartosc();
                        }
                        if (wartosckartaplayer1 > wartosckartaplayer2) {
                            player1.kartyplayer.addAll(stol.kupkaPlayer1);
                            player1.kartyplayer.addAll(stol.kupkaPlayer2);
                        } else {
                            player2.kartyplayer.addAll(stol.kupkaPlayer1);
                            player2.kartyplayer.addAll(stol.kupkaPlayer2);
                        }
                        licznikWojen++;
                    } catch (IndexOutOfBoundsException aioobe) {
                        System.out.println("Gracze nie maja kart" + aioobe);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


            }
            if (player1.kartyplayer.size() > 0) {
                player1.wygrany = true;
                System.out.println("Player1 " + player1.name + " wygrał w " + licznik + " rundach" + licznikWojen);
            } else {
                player2.wygrany = true;
                System.out.println("Player2 " + player1.name + " wygrał w " + licznik + " rundach");
            }

            System.out.println("Liczba wojen: " + licznikWojen);
        }       while(true);
    }

    private static void losujKarty(Player player1, Player player2, Deck deck, Random random) {
        while (deck.listaKart.size() > 0) {
            int losowyindeks = random.nextInt(deck.listaKart.size());
            Karta karta = deck.listaKart.get(losowyindeks);
            player1.kartyplayer.add(karta);
            deck.listaKart.remove(losowyindeks);
            losowyindeks = random.nextInt(deck.listaKart.size());
            karta = deck.listaKart.get(losowyindeks);
            player2.kartyplayer.add(karta);
            deck.listaKart.remove(losowyindeks);
        }
    }

}
