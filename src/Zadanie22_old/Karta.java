package Zadanie22_old;

public class Karta  {
    protected int wartosc;
    protected Kolor kolor;


    public Karta(int wartosc, Kolor kolor) {
        this.wartosc = wartosc;
        this.kolor = kolor;

    }

    public int getWartosc() {
        return wartosc;
    }

    public void setWartosc(int wartosc) {
        this.wartosc = wartosc;
    }

    public Kolor getKolor() {
        return kolor;
    }

    public void setKolor(Kolor kolor) {
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return "Karta{" +
                "wartosc=" + wartosc +
                ", kolor=" + kolor +
                '}';
    }
}
