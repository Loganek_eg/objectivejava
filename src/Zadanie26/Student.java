package Zadanie26;

public class Student  {
    /*
    Zadanie 26:
    a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
    b. Utwórz kilku studentów i dodaj do mapy (HashMap).
    c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
    d. wypisz studenta o indeksie 100400
    e. wypisz liczbę studentów
    f. wypisz wszystkich studentów
    g. zmień implementacje na TreeMap
    h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
            void addStudent(long indexNumber, String name, String surname)
            (możesz generować indexNumber)
            boolean containsStudent(long indexNumber)
            Student getStudent(long indexNumber)
            int studentsCount()
            void printAllStudent()
    i*. dodaj wyjątek NoSuchStudentException (RuntimeException), dodaj go do metody getStudent()
    j*. dodaj interfejs StudentFormat { String format(Student student) } i wykorzystaj go jako parametr w metodzie void printAllStudent()
    k. Wykonaj jeszcze raz zadanie pierwsze wykorzystując klasę University
     */
    protected Long numerIndeksu;
    protected String name;
    protected String surname;

    public Student(Long numerIndeksu, String name, String surname) {
        this.numerIndeksu = numerIndeksu;
        this.name = name;
        this.surname = surname;
    }

    public Long getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(Long numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numerIndeksu=" + numerIndeksu +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }


}
