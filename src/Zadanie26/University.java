package Zadanie26;

import java.util.HashMap;
import java.util.Map;

public class University {
    /*
h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
            void addStudent(long indexNumber, String name, String surname)
            (możesz generować indexNumber)
            boolean containsStudent(long indexNumber)
            Student getStudent(long indexNumber)
            int studentsCount()
            void printAllStudent()
 */

    private Map<Long, Student> mapaStudentow = new HashMap();


    public void addStudent( Student student) {
        mapaStudentow.put(student.getNumerIndeksu(),student);
    }

    public boolean containsStudent(long numerIndeksu) {
        return true;
    }

    public Student getStudent(long numerIndeksu) {

        return mapaStudentow.get(numerIndeksu);
    }

    public int studentsCount() {
        int licznik = 0;
        for (Long indeks : mapaStudentow.keySet()) {
            licznik++;
        }
        return licznik;
    }

    public void printAllStudents() {
        for (Student student : mapaStudentow.values()) {
            System.out.println(student);
        }
    }
    public int sizeOfStudents(){
        return  mapaStudentow.size();
    }


}
