package Zadanie26;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        University university = new University();
        Map<Long, Student> mapa = new HashMap<>();


        Student student1 = new Student(100200l, "Tomek", "Nazwisko");
        Student student2 = new Student(100400l, "Bartek", "Nazwisko");
        Student student3 = new Student(100600l, "Arek", "Nazwisko");
        Student student4 = new Student(100800l, "Maciek", "Nazwisko");

        university.addStudent(student1);
        university.addStudent(student2);
        university.addStudent(student3);
        university.addStudent(student4);


        for (Map.Entry<Long, Student> entry : mapa.entrySet()) {
            Long klucz = entry.getKey();
            Student value = entry.getValue();
            System.out.println("Klucz: " + entry.getKey() + " wartosc: " + entry.getValue());
        }
        System.out.println(university.getStudent(100200l));
        System.out.println(university.sizeOfStudents());
        university.printAllStudents();

    }


}
