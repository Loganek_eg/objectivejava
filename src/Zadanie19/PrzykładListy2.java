package Zadanie19;

import java.util.ArrayList;

public class PrzykładListy2 {
    public static void main(String[] args) {
        // lista dowolnego typu
        ArrayList lista = new ArrayList();

        lista.add(1.0);
        lista.add("jakis string");
        lista.add(null);

        // int -> Integer
        // double -> Double
        // boolean -> Boolean
        // float -> Float
        ArrayList<Integer> lista_intow = new ArrayList<>();
        lista_intow.add(1);         // indeks 0
        lista_intow.add(null);      // indeks 1
        lista_intow.add(2);         // indeks 2

        printList(lista_intow);
        //
        Integer obiekt = 5;
        lista_intow.add(1, obiekt);

        System.out.println(lista_intow);
        printList(lista_intow);


        lista_intow.remove(obiekt); // podając obiekt do usuniecia
        printListForEach(lista_intow);
        lista_intow.remove(1); // podając indeks obiektu usuwanego
        printListForEach(lista_intow);

//        while (lista_intow.contains(obiekt)){
//            lista_intow.remove(obiekt);
//        }


        ArrayList<Student> lista_studentow = new ArrayList<>();
        lista_studentow.add(new Student());

        //
        System.out.println(lista_studentow.get(0).getName());
        lista_studentow.remove(0);

//        System.err.println("Tu bedzie blad");
        //
//        System.out.println(lista_studentow.get(0).getName());


    }

    private static void printList(ArrayList<Integer> lista_intow) {
        System.out.println("----->");
        System.out.println("Jest " + lista_intow.size() + " elementow w liscie");
        for (int i = 0; i < lista_intow.size(); i++) {
//            System.out.println("Indeks " + i + " wartosc: " + lista_intow[i]);
            System.out.println("Indeks " + i + " wartosc: " + lista_intow.get(i));
        }
    }


    private static void printListForEach(ArrayList<Integer> lista_intow) {
        System.out.println("----->");
        System.out.println("Jest " + lista_intow.size() + " elementow w liscie");
        for (Integer value : lista_intow) {   // foreach
            System.out.println("Wartosc: " + value);
        }
    }



}