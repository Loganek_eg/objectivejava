package Zadanie19_;

public class Daughter extends FamilyMember {
    public Daughter() {
    }

    @Override
    public void introduce(){
        System.out.println("I am daughter in this family member");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
