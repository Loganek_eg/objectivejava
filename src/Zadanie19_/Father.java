package Zadanie19_;

public class Father extends  FamilyMember {
    public Father() {
    }

    @Override
    public void introduce(){
        System.out.println("I am father in this family member");
    }
    @Override
    public boolean isAdult() {
        return true;
    }
}
