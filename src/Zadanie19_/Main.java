package Zadanie19_;

import Zadanie1.Person;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        FamilyMember person = new FamilyMember() {
            @Override
            public boolean isAdult() {
                return false;
            }
        };
        ArrayList<FamilyMember> rodzina = new ArrayList<>();

        Mother m = new Mother();
        Son s = new Son();
        Daughter d = new Daughter();
        Father f = new Father();


        rodzina.add(new Mother());
        rodzina.add(new Daughter());
        rodzina.add(new Son());
        rodzina.add(new Father());

        for (FamilyMember member : rodzina) {
            member.introduce();
        }
        for (FamilyMember member : rodzina) {
            member.getAge();
        }


    }
}
