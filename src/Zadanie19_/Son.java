package Zadanie19_;

public class Son extends  FamilyMember {
    public Son() {
    }

    @Override
    public void introduce(){
        System.out.println("I am son in this family member");
    }
    @Override
    public boolean isAdult() {
        return false;
    }
}
