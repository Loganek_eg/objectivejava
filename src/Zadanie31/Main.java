package Zadanie31;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
/*
Zadanie 31:
    Stwórz aplikację która na wciśnięcie klawisza "Enter"
    wypisuje obecną datę i godzinę w (wybranym przez Ciebie) formacie.
    Jeśli użytkownik wpisze 'quit' to zakoncz program.
 */
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss:SSS");
        Scanner sc = new Scanner(System.in);
        String inputLine = "";
        do {
            System.out.println("Nacisnij Enter a podam Ci datę");
            inputLine = sc.nextLine();
            LocalDateTime data = LocalDateTime.now();
            System.out.println(data.format(format));
        } while (!inputLine.equals("quit"));
    }
}
