package Zadanie17;

public class Main {
    public static void main(String[] args) {


        Club club = new Club();
        Person person1 = new Person(15, "Antos", "Mrowka");
        Person person2 = new Person(14, "Antos", "Mrowka");
        Person person3 = new Person(32, "Antos", "Mrowka");
        Person person4 = new Person(91, "Antos", "Mrowka");
        Person person5 = new Person(13, "Antos", "Mrowka");
        try {
            club.enter(person1);
            club.enter(person2);
            club.enter(person3);
            club.enter(person4);
            club.enter(person5);
        } catch (NoAdultException nae) {
            System.out.println("Jesteś szczylem" + nae);
        }
        System.out.println(club.listaPersonow);
    }
}
