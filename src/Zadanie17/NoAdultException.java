package Zadanie17;

public class NoAdultException extends RuntimeException {

    /*
    Zadanie 17**:
    Napisz klasę Person, Club, NoAdultException.
        a.  Klasa Person powinna zawierać, imię, nazwisko i wiek
        b.  Klasa Club powinna zawierać metodę enter(Person person),
        która wyrzuca wyjątek NoAdultException, jeżeli osoba jest niepełnoletnia.
     */

    public NoAdultException() {
         super("Wypierdalaj gnoju!");
    }
}
