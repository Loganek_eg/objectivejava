package Zadanie28;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<Integer, Boolean> mapa = new HashMap<>();

        for (int i = 1; i <= 30; i++) {
            boolean jestpierwsza = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    jestpierwsza = false;
                }
            }
            mapa.put(i, jestpierwsza);

        }
        System.out.println("Liczby pierwsze to: ");
        for (Map.Entry<Integer, Boolean> entry : mapa.entrySet()) {
            Integer klucz = entry.getKey();
            Boolean value = entry.getValue();

            if (value == true) {
                System.out.print(klucz+" jest pierwsza \n");
            }
            else {
                System.out.println(klucz);
            }
        }


    }
}

