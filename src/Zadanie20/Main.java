package Zadanie20;

import javafx.scene.shape.Circle;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<Figure> listaFigur = new ArrayList<>();

        listaFigur.add(new Kwadrat(3));
        listaFigur.add(new Prostokąt(5,3));
        listaFigur.add(new Koło(5));

        for (Figure figura: listaFigur ) {
            if (figura instanceof Koło){

            }

            System.out.println(figura.obliczObwód());
            System.out.println(figura.obliczPole());
        }

    }
}
