package Zadanie20;

public class Kwadrat extends Figure {

    private int a;

    public Kwadrat(int a) {
        this.a= a;
    }

    @Override
    public double obliczPole() {
        int suma = a*a;
        return suma;
    }

    @Override
    public double obliczObwód() {
        int suma = 4*a;
        return suma;
    }
}
