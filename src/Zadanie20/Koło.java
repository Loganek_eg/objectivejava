package Zadanie20;

public class Koło extends Figure {

    private int r;
    private double p = 3.14;

    public Koło(int r) {
        this.r = r;
    }

    @Override
    public double obliczPole() {
        double suma = Math.pow(((Math.PI)*r),(2));
        return suma;
    }

    @Override
    public double obliczObwód() {
        double suma = 2*(Math.PI)*r;
        return suma;
    }
}
