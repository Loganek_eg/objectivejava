package Zadanie20;

public class Prostokąt extends Figure {

    private int a;
    private int b;

    public Prostokąt(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double obliczPole() {
        int suma = a * b;
        return suma;
    }

    @Override
    public double obliczObwód() {
        int suma;
        return suma = (2 * a) + (2 * b);
    }
}
