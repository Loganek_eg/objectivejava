package Zadanie20;

public abstract class Figure {

    /*
    Zadanie 20:
    Stwórz klasę nadrzędną Figure która posiada dwie metody abstrakcyjne:
        - metoda obliczPole
        - metoda obliczObwód
        Stwórz klasy podrzędne "Kwadrat", "Prostokąt", "Koło" które dziedziczą po Figure i
        zaimplementuj w nich metody obliczObwód i obliczPole. Stwórz maina, w tym mainie stwórz Listę
        obiektów Figure i dodaj kilka figur, a następnie w pętli foreach wypisz pola i obwody wszystkich figur.
        ** Zastosuj instanceof aby sprawdzić typ figury i wypisać dodatkowy komunikat o tym jaką figurę wypisujesz.
     */

    public abstract double obliczPole();
    public abstract double obliczObwód();


}
