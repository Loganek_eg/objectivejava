package Zadanie30;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 30 (Time):
    Stwórz aplikacje która ze scannera przyjmuje datę w formacie '2013-03-20 12:40'
    (ROK-MSC-DZIEN GODZINA:DATA) i wypisuje komunikat ze data jest w poprawnym formacie lub
    (jeśli nie jest) rzuca Exception "WrongDateTimeException" który jest wyjątkiem, który
     dziedziczy po RuntimeException (napisz go sam/a).

    dodatkowe.** Zmodyfikuj aplikację lub utwórz podobną aplikację, która na początku prosi o
     format daty (np. yyyy-mm-dd HH:mm) a po podaniu formatu daty
                działa tak jak poprzednia aplikacja, czyli przyjmuje datę w podanym formacie
                (tym wpisanym na początku aplikacji).
         */
        LocalDateTime localDateTime = LocalDateTime.now();
        Scanner sc =new Scanner(System.in);
        String inputLine="";
        String formatdaty="";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatdaty);

       do{

           System.out.println("Podaj format daty: Rok yyyy, Miesiac MM, Dzien dd, Godzina HH, Minuty mm, Sekundy ss");
           formatdaty = sc.nextLine();
           System.out.println("Podaj datę w formacie ROK-MSC-DZIEN GODZINA:DATA ");
           inputLine = sc.nextLine();
           if(inputLine.equals("quit")){
               break;
           }
           try{
              localDateTime = LocalDateTime.parse(inputLine, formatter);
               System.out.println(localDateTime);
           }catch (Exception de){
               System.out.println("Zły format daty" + de);
           }

       }while (!inputLine.equals("quit"));
    }
}
