package Zadanie32;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
        Zadanie 32:
    Stwórz aplikację która na:
        - wpisanie date wypisuje obecny LocalDate
        - wpisanie time wypisuje obecny LocalTime
        - wpisanie datetime wypisuje obecny LocalDateTime
        (W wybranym przez Ciebie formacie)
    Jeśli użytkownik wpisze 'quit' to zakoncz program.
         */
        Scanner sc = new Scanner(System.in);
        LocalDateTime dataiczas = LocalDateTime.now();
        LocalDate data = LocalDate.now();
        LocalTime czas = LocalTime.now();
        DateTimeFormatter formatdatyiczasu = DateTimeFormatter.ofPattern("dd/MM/YYYY, HH:mm:ss");
        DateTimeFormatter formatdaty = DateTimeFormatter.ofPattern("dd/MM/YYYY");
        DateTimeFormatter formatczasu = DateTimeFormatter.ofPattern("HH:mm:ss:SS");
        String inputLine= "";
        do{
            System.out.println("Wpisz komendy date/time/datetime aby otrzymac wynik");
            inputLine=sc.nextLine();
            if(inputLine.equals("datetime")){
                System.out.println(dataiczas.format(formatdatyiczasu));
            }
            else if(inputLine.equals("date")){
                System.out.println(data.format(formatdaty));
            }
            else if(inputLine.equals("time")){
                System.out.println(czas.format(formatczasu));
            }
            else{
                System.out.println("Zła komenda");
            }
        }while(!inputLine.equals("quit"));
    }
}
