package Zadanie21;

public class Soldier extends Citizen {

    protected String name = "Żołnierz";

    public Soldier(String name) {
        super(name);
    }

    public boolean canVote() {

        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
