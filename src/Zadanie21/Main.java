package Zadanie21;

public class Main {
    public static void main(String[] args) {
        Town town = new Town();
        town.addCitizen(new King("Król"));
        town.addCitizen(new Soldier("Soldier1"));
        town.addCitizen(new Soldier("Soldier2"));
        town.addCitizen(new Townsman("Mieszczanin1"));
        town.addCitizen(new Townsman("Mieszczanin2"));
        town.addCitizen(new Peasant("Wiesniak1"));
        town.addCitizen(new Peasant("Wiesniak2"));
        System.out.println(town.listaMieszkancow);

        System.out.println("Osobny które mogą głosować" + town.whoCanVote());
        System.out.println("Może głosować " + town.howManyCanVote() + " osób");
    }
}
