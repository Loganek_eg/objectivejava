package Zadanie21;

public class Peasant extends Citizen {


    public Peasant(String name) {
        super(name);
    }

    @Override
    public boolean canVote() {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
