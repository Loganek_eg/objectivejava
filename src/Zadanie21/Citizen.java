package Zadanie21;

public abstract class Citizen extends Town {
    protected boolean mozeGlosowac = true;

    protected Citizen() {
    }

    /*
    Zadanie 21:
    Stwórz klasę Citizen oraz klasy dziedziczące
        Peasant(Chłop),
        Townsman(Mieszczanin),
        King(Król),
        Soldier(Żołnierz)
    Wszystkie klasy posiadają pole imie
    Citizen powinien być klasą abstrakcyjną która posiada metodę abstrakcyjną 'canVote' która zwraca true dla
        townsman'a i soldier'a, ale false dla chłopa i króla.
    Stwórz klasę Town która posiada listę Citizenów. Dodaj do niej kilku citizenów (różnych w mainie) i
    stwórz metody howManyCanVote które zwracają ilość osób które mogą głosować.
    Stwórz w klasie Town metodę "whoCanVote" która zwraca imiona osób które mogą głosować.
     */
    public abstract boolean canVote();

    protected String name;
    protected boolean moze;
    public Citizen(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
