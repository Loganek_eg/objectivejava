package Zadanie21;

public class Townsman extends Citizen {
    protected String name = "Mieszczanin";

    public Townsman(String name) {
        super(name);
    }

    @Override
    public boolean canVote() {
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
