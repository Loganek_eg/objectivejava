package Zadanie21;

import java.util.ArrayList;

public class Town {


    ArrayList<Citizen> listaMieszkancow = new ArrayList<>();

    public void addCitizen(Citizen citizen) {
        listaMieszkancow.add(citizen);
    }

    public ArrayList<Citizen> getListaMieszkancow() {
        return listaMieszkancow;
    }

    public void setListaMieszkancow(ArrayList<Citizen> listaMieszkancow) {
        this.listaMieszkancow = listaMieszkancow;
    }

    public ArrayList<Citizen> whoCanVote() {
        ArrayList<Citizen> whocan = new ArrayList<>();
        for (Citizen citizen : listaMieszkancow) {
            if (citizen.canVote()) {
                whocan.add(citizen);
            }

        }

        return whocan;
    }

    public int howManyCanVote() {
        int licznik = 0;
        for (Citizen citizen : listaMieszkancow) {
            if (citizen.canVote()) {
                licznik++;

            }
        }
        return licznik;
    }

}
